# client black and white printer preset
the_name="bandw"
file_name="bandw.dmg"
p1="com.apple.print.custompresets.forprinter.w7545p.plist"
p2="com.apple.print.custompresets.forprinter.xc560.plist"
url="http://w.somethingtechnical.net/bandw.dmg"
dateM=$(date +%m-%d-%Y_%H%M%S)
bn=$(basename $0"_")
log_file="/Users/Shared/.labtech/logs/$bn$the_name_$dateM.txt"
a=$(stat -f "%Su" /dev/console)
if [ $a == "root" ]; then
	a=$(last | grep console | awk '{print $1}' | sed '/root/d' | head -1)
fi
pref_path="/Users/$a/Library/Preferences/"


if [ -e "/Users/Shared/.labtech/logs/bwp.inst" ]
	then echo "Presets are already installed, exiting."
	exit
fi


curl -L -o /tmp/$file_name $url
b="file downloaded."
hdiutil mount -nobrowse /tmp/$file_name
c="dmg mounted."
cp /Volumes/bandw/$p1 /Users/$a/Library/Preferences/$p1
cp /Volumes/bandw/$p2 /Users/$a/Library/Preferences/$p2
d="copied preset files."
chown $a:staff /Users/$a/Library/Preferences/$p1
chown $a:staff /Users/$a/Library/Preferences/$p2
e="set permissions."
killall -u $a cfprefsd
f="killed cfprefsd."
# fix chrome
cp /Volumes/bandw/bwp.scpt /Users/Shared/.labtech/apps/bwp.scpt
cp /Volumes/bandw/com.it360.bwp.plist /Users/$a/Library/LaunchAgents/com.it360.bwp.plist
chown -R $a:staff /Users/Shared/.labtech/apps/bwp.scpt
chown -R $a:staff /Users/$a/Library/LaunchAgents/com.it360.bwp.plist
g="copied chrome fix and fixed it's permissions."
#
hdiutil detach /Volumes/$the_name
h="unmounted dmg."
rm /tmp/$file_name
i="removed dmg."
touch /Users/Shared/.labtech/logs/bwp.inst

if [ -e $pref_path$p1 ]
	then
	aa="w7545p black and white preset was installed."
	ab=True
else
	aa="w7545p black and white preset wasn't installed."
	ab=False
fi
if [ -e $pref_path$p2 ]
	then
	bb="xc560 black and white preset was installed."
	bc=True
else
	bb="xc560 black and white preset wasn't installed."
	bc=False
fi
if [ -e /Users/Shared/.labtech/apps/bwp.scpt ]
	then
	cc="bwp.scpt is set."
	cd=True
else
	cc="bwp.scpt isn't set."
	cd=False
fi
if [ -e /Users/$a/Library/LaunchAgents/com.it360.bwp.plist ]
	then
	dd="bwp LaunchAgent is set."
	de=True
else
	dd="bwp LaunchAgent isn't set."
	de=False
fi

if [ $ab = False ]
	then the_success="[Error], something went wrong and the script failed."
else
	if [ $bc = False ]
		then the_success="[Error], something went wrong and the script failed."
	else
		if [ $cd = False ]
			then the_success="[Error], something went wrong and the script failed."
		else
			if [ $de = False ]
				then the_success="[Error], something went wrong and the script failed."
			else
				the_success="[Success], the script installed the black and white printer preset."
			fi
		fi
	fi
fi


echo $b > $log_file
echo $c >> $log_file
echo $d >> $log_file
echo $e >> $log_file
echo $f >> $log_file
echo $g >> $log_file
echo $h >> $log_file
echo $i >> $log_file
echo "=========" >> $log_file
echo $aa >> $log_file
echo $bb >> $log_file
echo $cc >> $log_file
echo $dd >> $log_file
echo $the_success >> $log_file

echo $b
echo $c
echo $d
echo $e
echo $f
echo $g
echo $h
echo $i
echo "========="
echo $aa
echo $bb
echo $cc
echo $dd
echo $the_success